#include "Dirt.h"

Dirt::Dirt()
{
	LoadContent();
}
Dirt::~Dirt()
{
	UnLoadContent();
}


void Dirt::LoadContent()
{
	dirt = al_load_bitmap("Images/Tiles/dirt.png");
}
void Dirt::UnLoadContent()
{
	al_destroy_bitmap(dirt);
}


void Dirt::Update(InputManager *input)
{

}
void Dirt::Draw(ALLEGRO_DISPLAY *display)
{
	al_draw_bitmap(dirt, pos.GetX(), pos.GetY(), NULL);
}

void Dirt::SetPosition(int x, int y, int w, int h)
{
	pos.SetValues(x, y, w, h);
}