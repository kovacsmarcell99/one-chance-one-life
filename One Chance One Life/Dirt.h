#ifndef DIRT_H
#define DIRT_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "Rect.h"
#include "InputManager.h"

class Dirt
{
public:
	Dirt();
	~Dirt();

	void LoadContent();
	void UnLoadContent();

	void Update(InputManager *input);
	void Draw(ALLEGRO_DISPLAY *display);

	void SetPosition(int x, int y, int w, int h);
private:
	ALLEGRO_BITMAP *dirt;
	Rect pos;
};

#endif