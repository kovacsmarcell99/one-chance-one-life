#include "Grass.h"

Grass::Grass()
{
	LoadContent();
}
Grass::~Grass()
{
	UnLoadContent();
}


void Grass::LoadContent()
{
	grass = al_load_bitmap("Images/Tiles/grass.png");
}

void Grass::UnLoadContent()
{
	al_destroy_bitmap(grass);
}


void Grass::Update(InputManager *input)
{

}

void Grass::Draw(ALLEGRO_DISPLAY * display)
{
	if (grass != NULL)
		al_draw_bitmap(grass, pos.GetX(), pos.GetY(), NULL);
}


void Grass::SetPosition(int x, int y, int w, int h)
{
	pos.SetValues(x, y, w, h);
}