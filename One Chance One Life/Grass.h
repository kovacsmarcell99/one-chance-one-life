#ifndef GRASS_H
#define GRASS_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "InputManager.h"
#include "Rect.h"

class Grass
{
public:
	Grass();
	~Grass();

	void LoadContent();
	void UnLoadContent();

	void Update(InputManager *input);
	void Draw(ALLEGRO_DISPLAY *display);

	void SetPosition(int x, int y, int w, int h);
private:
	ALLEGRO_BITMAP *grass;
	Rect pos;
};

#endif