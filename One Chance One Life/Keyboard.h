#ifndef KEYBOARD_H
#define	KEYBOARD_H

#include <allegro5/allegro.h>

class Keyboard
{
public:
	bool GetKeyPress(int key)
	{
		if (al_key_down(&keyState, key))
			return true;
		else
			return false;
	}

	void Update()
	{
		al_get_keyboard_state(&keyState);
	}
private:
	ALLEGRO_KEYBOARD_STATE keyState;
};

#endif