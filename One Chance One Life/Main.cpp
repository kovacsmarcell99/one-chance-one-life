#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include<allegro5\allegro_audio.h> 
#include<allegro5\allegro_acodec.h>
#include "ScreenManager.h"
#include "InputManager.h"

#define ScreenWidth 800
#define ScreenHeight 600

int main()
{
	al_init();
	al_init_image_addon();
	
	al_install_keyboard();

	al_install_audio();
	al_init_acodec_addon();

	al_reserve_samples(1);



	ALLEGRO_DISPLAY *display;
	display = al_create_display(ScreenWidth,ScreenHeight);

	if (display == NULL)
	{
		return 1;
	}

	al_set_window_title(display, "Marcell Games - One Chance One Life(Run Yellow Run)");
	al_set_display_icon(display, al_load_bitmap("Images/icon.png"));

	ALLEGRO_SAMPLE *song = al_load_sample("Music/music.ogg");

	ALLEGRO_SAMPLE_INSTANCE *songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());


	bool running = true;

	ALLEGRO_EVENT_QUEUE *event_queue;
	event_queue = al_create_event_queue();

	float FPS = 60.0f;

	ALLEGRO_TIMER *timer;
	timer = al_create_timer(1.0f / FPS);

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	al_start_timer(timer);

	InputManager *input = new InputManager;

	ScreenManager screen;

	al_play_sample_instance(songInstance);

	while (running)
	{
		al_wait_for_event(event_queue, &input->event);

		if (input->event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			running = false;

		input->key.Update();

		if (input->event.type = ALLEGRO_EVENT_TIMER)
		{
			//Update
			screen.Update(input);
			//Draw
			screen.Draw(display);
		}

		al_flip_display();
		al_clear_to_color(al_map_rgb(33, 103, 255));
	}

	delete input;

	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);

	return 0;
}