#include "Player.h"

Player::Player()
{
	LoadContent();
}
Player::~Player()
{
	UnLoadContent();
}


void Player::LoadContent()
{
	player = al_load_bitmap("Images/player.png");
	pos.SetValues(50, 400, 40, 100);
	onGround = false;
	groundY = 500;

	dead = false;

	jumped = false;
	up = false;

	upLimit = 230;
}
void Player::UnLoadContent()
{
	al_destroy_bitmap(player);
}


void Player::Update(InputManager *input)
{
	if (!dead)
	{

		if (input->key.GetKeyPress(ALLEGRO_KEY_RIGHT) && (pos.GetX() + pos.GetW()) < 800)
			pos.SetX(pos.GetX() + 3);
		if (input->key.GetKeyPress(ALLEGRO_KEY_LEFT) && pos.GetX() > 0)
			pos.SetX(pos.GetX() - 3);
		if (input->key.GetKeyPress(ALLEGRO_KEY_UP) && onGround)
			up = true;

		if (up)
		{
			if (upLimit < pos.GetY())
				pos.SetY(pos.GetY() - 5);
			else
				up = false;
		}

		if (pos.GetY() + pos.GetH() < groundY)
			onGround = false;
		else
			onGround = true;

		if (!onGround && !up)
		{
			pos.SetY(pos.GetY() + 5);
		}
	}
	if (dead)
	{
		jumped = false;
		up = false;
		onGround = true;
	}
}
void Player::Draw(ALLEGRO_DISPLAY *display)
{
	al_draw_bitmap(player, pos.GetX(), pos.GetY(), NULL);
}


void Player::Collision(Rect rect)
{
	if (pos.Intersects(rect))
		dead = true;
}


bool Player::Dead()
{
	return dead;
}


void Player::SetDead(bool dead)
{
	this->dead = dead;
}

void Player::SetPosition(int x, int y, int w, int h)
{
	pos.SetValues(x, y, w, h);
}