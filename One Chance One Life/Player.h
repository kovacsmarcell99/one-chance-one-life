#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "InputManager.h"
#include "Rect.h"

class Player
{
public:
	Player();
	~Player();

	void LoadContent();
	void UnLoadContent();

	void Update(InputManager *input);
	void Draw(ALLEGRO_DISPLAY *display);

	void Collision(Rect rect);

	bool Dead();

	void SetDead(bool dead);

	void SetPosition(int x, int y, int w, int h);
private:
	ALLEGRO_BITMAP *player;
	Rect pos;
	int groundY;
	bool jumped;
	bool onGround;

	bool dead;

	bool up;
	int upLimit;
};

#endif