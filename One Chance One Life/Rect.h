#ifndef RECT_H
#define RECT_H

class Rect
{
public:
	Rect()
	{
		Clear();
	}
	Rect(int x, int y, int w, int h)
	{
		SetValues(x,y,w,h);
	}

	void Clear()
	{
		SetValues(0, 0, 0, 0);
	}

	void SetValues(int x, int y, int w, int h)
	{
		this->x = x;
		this->y = y;
		this->w = w;
		this->h = h;
	}

	void SetX(int x)
	{
		this->x = x;
	}
	void SetY(int y)
	{
		this->y = y;
	}
	void SetW(int w)
	{
		this->w = w;
	}
	void SetH(int h)
	{
		this->h = h;
	}

	int GetX()
	{
		return x;
	}
	int GetY()
	{
		return y;
	}
	int GetW()
	{
		return w;
	}
	int GetH()
	{
		return h;
	}

	bool Intersects(Rect rect)
	{
		//The sides of the rectangles
		int leftA, leftB;
		int rightA, rightB;
		int topA, topB;
		int bottomA, bottomB;

		//Calculate the sides of rect A
		leftA = x;
		rightA = x + w;
		topA = y;
		bottomA = y + h;

		//Calculate the sides of rect B
		leftB = rect.GetX();
		rightB = rect.GetX() + rect.GetW();
		topB = rect.GetY();
		bottomB = rect.GetY() + rect.GetH();

		if (bottomA <= topB)
			return false;
		if (topA >= bottomB)
			return false;
		if (rightA <= leftB)
			return false;
		if (leftA >= rightB)
			return false;

		return true;
	}
private:
	int x, y, w, h;
};

#endif