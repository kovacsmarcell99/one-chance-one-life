#include "ScreenManager.h"

ScreenManager::ScreenManager()
{
	LoadContent();
}
ScreenManager::~ScreenManager()
{
	UnLoadContent();
}

void ScreenManager::LoadContent()
{
	for (int i = 0; i < 16; i++)
	{
		grasses[i].SetPosition(i * 50, 500, 50, 50);
		dirts[i].SetPosition(i * 50, 550, 50, 50);
		if (i < maxSpike)
			spikes[i].SetPosition(-51, 470, 50, 30);
	}
	spikeNum = 0;
	timeLimit = 120;
	point = 0;
	showedPoints = false;
	drawingMessageBox = false;
}
void ScreenManager::UnLoadContent()
{

}


void ScreenManager::Update(InputManager *input)
{
	if (input->key.GetKeyPress(ALLEGRO_KEY_R) && player.Dead())
	{
		showedPoints = false;
		time = 0;
		point = 0;
		player.SetDead(false);
		for (int i = 0; i < 8; i++)
		{
			spikes[i].SetPosition(-51, 470, 50, 30);
		}
		player.SetPosition(50, 400, 40, 100);
		timeLimit = 120;
	}
	player.Update(input);
	for (int i = 0; i < 16; i++)
	{
		grasses[i].Update(input);
		dirts[i].Update(input);
		if (i < maxSpike)
		{
			spikes[i].Update(input,player.Dead());
			player.Collision(spikes[i].GetPosition());
		}
	}
	if (maxSpike - 1 < spikeNum)
		spikeNum = 0;

	if (timeLimit < 50)
		timeLimit = 50;

	//Level Generator
	if (time > timeLimit)
	{
		time = 0;
		if (!player.Dead())
		{
			spikes[spikeNum].SetPosition(800, 470, 50, 30);
			spikeNum++;
		}
		timeLimit -= 2;
	}

	if (!player.Dead())
	{
		std::cout << "Points:" << point << std::endl;
		point++;

		if (time > 60 * 60 * 60)
			time = 0;
		time++;
	}
	if (player.Dead() && !showedPoints)
	{
		showedPoints = true;
		drawingMessageBox = true;
	}
		
}
void ScreenManager::Draw(ALLEGRO_DISPLAY *display)
{
	player.Draw(display);
	for (int i = 0; i < 16; i++)
	{
		grasses[i].Draw(display);
		dirts[i].Draw(display);
		if (i < maxSpike)
			spikes[i].Draw(display);
	}
	if (drawingMessageBox)
	{
		drawingMessageBox = false;
		al_show_native_message_box(display, "You died!", "", "YOU DIED! \n \n Press R to Respawn!",NULL,NULL);
	}
}