#ifndef SCREENMANAGER_H
#define SCREENMANAGER_H

#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro.h>
#include "InputManager.h"
#include "Player.h"
#include "Spike.h"
#include "Grass.h"
#include "Dirt.h"

class ScreenManager
{
public:
	ScreenManager();
	~ScreenManager();

	void LoadContent();
	void UnLoadContent();

	void Update(InputManager *input);
	void Draw(ALLEGRO_DISPLAY *display);
private:
	Player player;
	Grass grasses[16];
	Dirt dirts[16];

	static const int maxSpike = 8;
	Spike spikes[maxSpike];
	int spikeNum;
	unsigned int time;
	int timeLimit;
	unsigned int point;

	bool showedPoints;
	bool drawingMessageBox;
};

#endif