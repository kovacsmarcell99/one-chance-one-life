#include "Spike.h"

Spike::Spike()
{
	LoadContent();
}

Spike::~Spike()
{
	UnLoadContent();
}

void Spike::LoadContent()
{
	spike = al_load_bitmap("Images/Tiles/spike.png");
	speed = 3;
}
void Spike::UnLoadContent()
{
	al_destroy_bitmap(spike);
}


void Spike::Update(InputManager *input,bool dead)
{
	if (!dead)
	{
		if (pos.GetX() + pos.GetW() > 0)
			pos.SetX(pos.GetX() - 5);
	}
}
void Spike::Draw(ALLEGRO_DISPLAY *display)
{
	al_draw_bitmap(spike, pos.GetX(), pos.GetY(), NULL);
}


void Spike::SetPosition(int x, int y, int w, int h)
{
	pos.SetValues(x, y, w, h);
}

void Spike::SetSpeed(int speed)
{
	this->speed = speed;
}


Rect Spike::GetPosition()
{
	return pos;
}