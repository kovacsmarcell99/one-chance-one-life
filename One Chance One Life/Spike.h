#ifndef SPIKE_H
#define SPIKE_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#include "Rect.h"
#include "InputManager.h"

class Spike
{
public:
	Spike();
	~Spike();

	void LoadContent();
	void UnLoadContent();

	void Update(InputManager *input, bool dead);
	void Draw(ALLEGRO_DISPLAY *display);

	void SetPosition(int x, int y, int w, int h);

	void SetSpeed(int speed);

	Rect GetPosition();
private:
	ALLEGRO_BITMAP *spike;
	Rect pos;
	int speed;
};

#endif